# Mobile-and-embedded-project
# Group members:  
                  - DELBEKE Julien
                  - RAFAELE Antonio
                  - RAMOS PEREZ Nestor Eduardo
                 
# Simulation setup
No simulation file is provided so you can design the simulation you want.
The border router has to be created first and then the other nodes may follow.

# Start server
To start the server, open a terminal window in the project directory and type:
    python3 server.py
    
# Serial Server Socket
For the serial server socket, the default 60001 port number is used.
