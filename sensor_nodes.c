#include "contiki.h"
#include "net/rime/rime.h"
#include "random.h"
#include "lib/list.h"
#include "dev/button-sensor.h"
#include "dev/leds.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "macros.h"
#include <string.h>

#define NO_MORE_MY_FATHER 3
#define PING_THRESHHOLD 30
/* Initiate variable */

struct node{
	struct node *son;
	linkaddr_t addr;			// Node address
	uint16_t last_rssi;			//The Received Signal Strength Indicator (RSSI)
};

/* The sons_list is a Contiki list that holds the sons seen thus far. */
LIST(sons_list);

/* Broadcast and unicast structure */
static struct broadcast_conn broadcast;
static struct unicast_conn uc;
static process_event_t valves_instructions;

/* Node father */
static struct node *father = NULL;			//Father node
static bool final_father=false;

/*For the Ping-Pong*/
static int ping_attempts = 0;
static int ping_timer = -1;

/* Connected directly or not to Border Router */
static bool connected_with_border = false;


/*---------------------------------------------------------------------------*/
/* All Process */
PROCESS(broadcast_process, "Broadcast process");
PROCESS(unicast_process, "Unicast process");
PROCESS(air_measurements_process, "Air measurement process");
PROCESS(valve_process, "Valve action");
AUTOSTART_PROCESSES(&broadcast_process,&unicast_process,&air_measurements_process, &valve_process);

/*---------------------------------------------------------------------------*/
//ALL FUNCTIONS
static void sending_message(int type, linkaddr_t node_address, const linkaddr_t * destination){
	uint8_t msg_to_send[3];
	msg_to_send[0] = type;
	msg_to_send[1] = node_address.u8[0];
	msg_to_send[2] = node_address.u8[1];
	packetbuf_copyfrom(msg_to_send,sizeof(msg_to_send));
	unicast_send(&uc, destination);

}

static void remove_all_children(){
	if(list_length(sons_list)!=0){
		struct node *son = list_head(sons_list);
		while(son!=NULL){
			sending_message(NO_MORE_MY_FATHER,linkaddr_node_addr,&son->addr);
			printf("Removed son %d\n",son->addr.u8[0]);
			son = list_item_next(son);
		}
		list_init(sons_list);
	}
}

static void look_for_new_father(){
	printf("Suppression of father %d\n",father->addr.u8[0]);
	father = NULL;
	final_father = false;
	connected_with_border=false;
	ping_timer = -1;
	ping_attempts = 0;
	remove_all_children();
	uint8_t message_to_send = DISCOVERY_SIGNAL;																//Look again for possible father
	packetbuf_copyfrom(&message_to_send, sizeof(message_to_send));
	broadcast_send(&broadcast);																				//Send broadcast to find father
	printf("broadcast message AGAIN sent to JOIN FATHER\n");
}

static bool already_son(uint8_t node_address){
	/*Ckecks if the node parameter is already in the son list*/
	if(list_length(sons_list)!=0){
		struct node *next_node = list_head(sons_list);
		while(next_node != NULL){
			if(next_node->addr.u8[0]==node_address){
				printf("Node %d in process list!\n",node_address);
				return true;
			}
			next_node = list_item_next(next_node);
		}
	}
	return false;
}

static void reset_ping_info(){
	ping_timer = 0;
	ping_attempts=0;
}
/*---------------------------------------------------------------------------*/
//Receiving structures
static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from){
	uint8_t message_received = *(uint8_t*)packetbuf_dataptr();
	if(connected_with_border && father->addr.u8[0]!=from->u8[0] && message_received==DISCOVERY_SIGNAL){
		sending_message(FATHER_SIGNAL,linkaddr_node_addr,from);										//Propose to be the father
	}
}

static void recv_uc(struct unicast_conn *c, const linkaddr_t *from){
	uint8_t *message_received = packetbuf_dataptr();												//fetch message
	//create node structure
	struct node *new_node = malloc(sizeof(struct node));											//Store information about the node communicating
	new_node->last_rssi = abs(packetbuf_attr(PACKETBUF_ATTR_RSSI));									//retrieve signal strength
	linkaddr_copy(&new_node->addr,from);
	//creation complete
	switch (message_received[0]) {
		case FATHER_SIGNAL:																			//Node request to become our father
			if(father==NULL){
				father = new_node;
			}
			else{
				if(!final_father && father->addr.u8[0] != new_node->addr.u8[0] && father->last_rssi > new_node->last_rssi){
					father = new_node;
				}
			}
		break;

		case ACK_FATHER_SIGNAL:
			if(connected_with_border && !already_son(from->u8[0])){
				list_add(sons_list,new_node); 															//Son added to list of son's
			}
			else{
				sending_message(NO_MORE_MY_FATHER,linkaddr_node_addr,&new_node->addr);
			}
		break;

		case NO_MORE_MY_FATHER:
			if(father->addr.u8[0]==from->u8[0]){
				printf("NO_MORE_MY_FATHER signal message received from %d\n",from->u8[0]);
				look_for_new_father();
			} 
		break;

		case PING_TO_FATHER :
			printf("PING_TO_FATHER received by son %d\n",from->u8[0]);
			sending_message(PONG_TO_SON,linkaddr_node_addr,from);
		break;

		case PONG_TO_SON:
			printf("PONG_TO_FATHER received by father %d\n",from->u8[0]);
			reset_ping_info();
		break;

		case VALVE_INSTRUCTION:
			printf("VALVE INSTRUCTION RECEIVED FROM %d and the message received is for %d\n",from->u8[0],message_received[2]);
			reset_ping_info();
			if(message_received[2] != linkaddr_node_addr.u8[0]){
				//Suppress itself from path
				int msg_to_send_length = packetbuf_datalen();		
				uint8_t message_to_send[msg_to_send_length-2];
				int iter;for(iter=0;iter<msg_to_send_length-2;iter++){message_to_send[iter]=message_received[iter];}
				//Send to next concerned son
				linkaddr_t concerned_son;
				concerned_son.u8[0] = message_to_send[msg_to_send_length-2];
				concerned_son.u8[1] = message_to_send[msg_to_send_length-1];
				packetbuf_copyfrom(message_to_send,sizeof(message_to_send));
				unicast_send(&uc, &concerned_son);
			}
			//If it is my Valve instruction
			else{
				process_post(&valve_process, valves_instructions, message_received[1]);
			}
		break;

		case AIR_DATA:
			printf("Air data received from %d\n",from->u8[0]);
			//Node add itself in the message.
			int msg_to_send_length = packetbuf_datalen();								//get message length
			uint8_t msg_to_send[msg_to_send_length+2];
			int iter;for(iter=0;iter<msg_to_send_length+2;iter++){msg_to_send[iter]=message_received[iter];}
			msg_to_send[msg_to_send_length] = linkaddr_node_addr.u8[0];
			msg_to_send[msg_to_send_length+1] = linkaddr_node_addr.u8[1];
			packetbuf_copyfrom(msg_to_send, sizeof(msg_to_send));										//As the node is not a computational node
			unicast_send(&uc,&father->addr);
		break;
	}

}
/*---------------------------------------------------------------------------*/
//Link structure to calls
static const struct broadcast_callbacks broadcast_call = {broadcast_recv};							//Broadcast structure. All broadcasted message received will be handle by the function broadcast_recv
static const struct unicast_callbacks unicast_callbacks = {recv_uc};								//Unicast structure. All unicast message received will be handle by the function uc_recv

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(broadcast_process, ev, data){
	/*Thread to find a father*/
	static struct etimer et;																		//Use to wait a amount of time
	static uint8_t message_to_send; 

	PROCESS_EXITHANDLER(broadcast_close(&broadcast);)

	PROCESS_BEGIN();

	broadcast_open(&broadcast, 129, &broadcast_call);												//Open listener to send the received paquet to the structure broadcast_call

	while(1) {

		etimer_set(&et, CLOCK_SECOND);
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
		if(father==NULL){
			message_to_send = DISCOVERY_SIGNAL;													//Search for a father
			packetbuf_copyfrom(&message_to_send, sizeof(message_to_send));								//Copy into a packet buffer
			broadcast_send(&broadcast);															//Send broadcast
			printf("Looking for a father. Broadcasting request.\n");
		}
	}
	PROCESS_END();
}

PROCESS_THREAD(unicast_process, ev, data){
	/*Thread to send a unicast message to a specific node*/
	static struct etimer et;																	//Use to wait a amount of time. 
	PROCESS_EXITHANDLER(unicast_close(&uc);)
	PROCESS_BEGIN();

	unicast_open(&uc, 146, &unicast_callbacks);													//Open listener to send the received paquet to the structure unicast_call

	while(1){
		etimer_set(&et, CLOCK_SECOND*(10 + (random_rand()%3)));									//Wait around 10-13 seconds seconds (depends on each node)
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
		if(father!=NULL && !final_father){
			sending_message(ACK_FATHER_SIGNAL,linkaddr_node_addr,&father->addr);
			final_father=true;
			connected_with_border=true;
			printf("Father found ! ACK_FATHER_SIGNAL sent to %d\n",father->addr.u8[0]);
			ping_timer=0;																		//Start pinging mechanism
		}

		if(ping_timer!=-1 && final_father && father!=NULL){
			ping_timer+=1;
			if(ping_timer>PING_THRESHHOLD){
				sending_message(PING_TO_FATHER,linkaddr_node_addr,&father->addr);
				ping_attempts+=1;
				if(ping_attempts>3 ){
					look_for_new_father();
				}
			}
		}

	}
	PROCESS_END();
}


PROCESS_THREAD(air_measurements_process, ev, data)						//Process handling the air measurements data exchange. 
{
	static struct etimer et;

	PROCESS_BEGIN();

	while(1) {
		etimer_set(&et, CLOCK_SECOND*60);								//Send air measurement analysis every 60 seconds
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
		if(father!=NULL && final_father){
			uint8_t msg_to_send[4];
			msg_to_send[0] = AIR_DATA;
			msg_to_send[1] = random_rand();									//All random for the moment
			msg_to_send[2] = linkaddr_node_addr.u8[0]; 
			msg_to_send[3] = linkaddr_node_addr.u8[1];
			packetbuf_copyfrom(msg_to_send, sizeof(msg_to_send));
			unicast_send(&uc, &father->addr);								//Send to father to reach computational node or border router.
			printf("My air data was sent to my father %d. The data is : %d\n",father->addr.u8[0],msg_to_send[1]);
		}
	}
	PROCESS_END();
}

PROCESS_THREAD(valve_process, ev, data)	/*Process to manage the valve and the LED*/
{
	static struct etimer et;

	PROCESS_BEGIN();
	leds_off(LEDS_ALL);
	while(1) {
		uint8_t instruction;

		PROCESS_WAIT_EVENT_UNTIL(ev == valves_instructions);
		printf("LEDS: %u\n", LEDS_GREEN);
		instruction = (uint8_t) data;
		if (instruction==OPEN_VALVE){
			leds_off(LEDS_GREEN);
			leds_on(LEDS_RED);

			printf("Valve instruction: %s\n", "OPEN_VALVE");
		}
		else{
			leds_off(LEDS_RED);
			leds_on(LEDS_GREEN);
			
			printf("Valve instruction: %s\n", "CLOSE_VALVE");
			
		}
	PROCESS_END();
	}
}
