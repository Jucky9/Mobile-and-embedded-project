#include "contiki.h"
#include "net/rime/rime.h"
#include "random.h"
#include "lib/list.h"
#include "dev/button-sensor.h"
#include "dev/leds.h"
#include <stdio.h>
#include "dev/serial-line.h"
#include "apps/antelope/debug.h"
#include "macros.h"
#include <string.h>

#define NO_MORE_MY_FATHER 14
/* Initiate variables */
struct node{
	struct node *son;			// All sons 
	linkaddr_t addr;			// Node address
	uint16_t last_rssi;			//The Received Signal Strength Indicator (RSSI)
};

/* The sons_list is a Contiki list that holds the sons seen thus far. */
LIST(sons_list);

/* Broadcast and unicast structure */
static struct unicast_conn unicast;
static struct broadcast_conn broadcast;
static uint8_t TAG;

/*---------------------------------------------------------------------------*/
PROCESS(broadcast_process, "Broadcast example");
PROCESS(unicast_process, "Example unicast");
PROCESS(receiving_instructions_process, "Serial line process");
AUTOSTART_PROCESSES(&broadcast_process, &unicast_process, &receiving_instructions_process);
/*---------------------------------------------------------------------------*/

static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from)
{
	PRINTF("SALUT BROADCAST RECEIVED\n");
	uint8_t msg_to_send[3];
	uint8_t *message_received = packetbuf_dataptr();
	PRINTF("broadcast message received from %d.%d: '%s'\n",from->u8[0], from->u8[1], message_received);
	switch (message_received[0]){
		case DISCOVERY_SIGNAL:
			PRINTF("DISCOVERY_SIGNAL received!\n");
			msg_to_send[0] = FATHER_SIGNAL ;							//Propose to be the father
			msg_to_send[1] = linkaddr_node_addr.u8[0];					//attach border router address
			msg_to_send[2] = linkaddr_node_addr.u8[1];
			packetbuf_copyfrom(msg_to_send,sizeof(msg_to_send));		//Register response into paquet buffer
			unicast_send(&unicast, from);
			PRINTF("Border router(%d.%d) RESPONSE to possible son %d.%d: '%d'\n",linkaddr_node_addr.u8[0],linkaddr_node_addr.u8[1], from->u8[0], from->u8[1], msg_to_send[0]);
		break;
	}
}
/*---------------------------------------------------------------------------*/
static const struct broadcast_callbacks broadcast_call = {broadcast_recv};		//Broadcast structure. All broadcasted message received will be handle by the function broadcast_recv
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(broadcast_process, ev, data)
{
	static struct etimer et;													//Use to wait a amount of time

	PROCESS_EXITHANDLER(broadcast_close(&broadcast);)
	PROCESS_EXITHANDLER(unicast_close(&unicast);)

	PROCESS_BEGIN();

	broadcast_open(&broadcast, 129, &broadcast_call);							//Open listener to send the received paquet to the structure broadcast_call

	while(1) {
		etimer_set(&et, CLOCK_SECOND);
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
	}

	PROCESS_END();
}

static void recv_uc(struct unicast_conn *c, const linkaddr_t *from)
{
	struct node *new_node = malloc(sizeof(struct node));
	PRINTF("unicast message received from %d.%d\n",from->u8[0], from->u8[1]);
	uint8_t msg_to_send[3];
	uint8_t *message_received =  packetbuf_dataptr();
	switch (message_received[0]){
		case ACK_FATHER_SIGNAL:													//Sending node confirmation for becoming a son
			PRINTF("ACK DU FILSton: %d.%d\n",from->u8[0], from->u8[1]);
			linkaddr_copy(&new_node->addr,from);
			list_add(sons_list,new_node); 															//Son added to list of son's
		break;

		case PING_TO_FATHER:
			PRINTF("received PING from Son %d\n",from->u8[0]);
			TAG = PONG_TO_SON;
			packetbuf_copyfrom(&TAG, sizeof(TAG));
			unicast_send(&unicast, from);
		break;

		case AIR_DATA:															//Air data analysis paquet received.
			PRINTF("NODE SRC---%d.%d\n",message_received[2],message_received[3]);
			PRINTF("Received AIR DATA from son %d\n",message_received[2]);
			PRINTF("AIR DATA is: %d\n",message_received[1]);					//As the node is not a computational node
			PRINTF("AirMeasurement_sent to SERVER\n");
			//Send message to server
			char message_to_send[100] = "<";
			char *space = " ";
			char buff[2];
			int i;
			int size = packetbuf_datalen();
			for (i=0;i<size;i++){
				strcat(message_to_send,space);
				sprintf(buff,"%d",message_received[i]);
				strcat(message_to_send,buff);
			}
			strcat(message_to_send," >");
			printf("%s\n",message_to_send);
		break;
	}
}
/*---------------------------------------------------------------------------*/
static const struct unicast_callbacks unicast_callbacks = {recv_uc};			//Unicast structure. All unicast message received will be handle by the function uc_recv
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(unicast_process, ev, data)
{
	static struct etimer et;											//Use to wait a amount of time. 
	PROCESS_EXITHANDLER(unicast_close(&unicast);)

	PROCESS_BEGIN();

	unicast_open(&unicast, 146, &unicast_callbacks);

	while(1) {
		etimer_set(&et, CLOCK_SECOND);
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
	}

	PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(receiving_instructions_process, ev, data)
{
	PROCESS_BEGIN();
	uint8_t msg_to_send[3];
	for(;;) {
		PROCESS_WAIT_EVENT_UNTIL(ev == serial_line_event_message);
		if(ev == serial_line_event_message) {
			//Find message_to_send length
			char data_copy[strlen(data)];			//create copy
			strcpy(data_copy, data);

			char * token = strtok(data, " ");
			int size = 0;
			while( token != NULL ) {
				size++;
				token = strtok(NULL, " ");
			}
			uint8_t message_to_send[size];
			int j=0;
			token = strtok(data_copy, " ");
			while( token != NULL ) {
				message_to_send[j] = (uint8_t)atoi(token);
				j++;
				token = strtok(NULL, " ");
			}
			//Message must be sent to the concerned son
			linkaddr_t concerned_son;
			concerned_son.u8[0] = message_to_send[size-2];
			concerned_son.u8[1] = message_to_send[size-1];
			packetbuf_copyfrom(message_to_send,sizeof(message_to_send));
			unicast_send(&unicast, &concerned_son);
		}
	}
	PROCESS_END();
}