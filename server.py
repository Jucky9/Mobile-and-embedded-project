import socket, datetime, time
from threading import Thread, Lock

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 60001        # Port to listen on (non-privileged ports are > 1023)

VALVE_INSTRUCTION=5	#Macro for the network
MIN_DATA_AMOUNT = 30	#Minimum number of air measurments before computing the slope
TIMEOUT = 600 		#10min in seconds
mutex = Lock()
status = {} 		#keys are node id of sensors and data a list of previous air data and a timestamp of the last change
THRESHOLD = 80 		#limit of air data before oppening the valve for 10 minutes
SLOPE_TRESHOLD = 0 	#if the slope is negative, we open the valve and if positive we close it

def assignData(line):
    mutex.acquire()
    try:							#parsing the received data
        data = line.split(" ")
        airQuality = int(data[2])
        nodeId = data[3]
        restOfData= data[4:-1]
        updateStatus(nodeId,airQuality,restOfData)
        timeoutCheck()
    finally:
        mutex.release()

def calculus(nodeId):						#computing the slope of the least fit square line	
    Y_AXIS = status[nodeId][0]
    X_AXIS = np.array(list(range(1, Y_AXIS.size+1)))
    A = np.vstack([X_AXIS, np.ones(len(X_AXIS))]).T
    slope, b = np.linalg.lstsq(A, Y_AXIS)[0]
    if slope<SLOPE_TRESHOLD:
        return True
    else:
        return False


def updateStatus(nodeId, airQuality, restOfData):
    openValve = False
  
    if nodeId not in status.keys():
    	status[nodeId] = [[],None] 					#create new entry in dictionnary status

    if len(status[nodeId][0]) < MIN_DATA_AMOUNT:
    	status[nodeId][0].append(airQuality)
    	if airQuality < THRESHOLD:
    		openValve = True
    else:
    	openValve = calculus(nodeId)					#computing the slope

    status[nodeId][1]=datetime.datetime.now() 				#update timestamp for the timeout
    time.sleep(0.5)
    sendToSensor(nodeId,openValve,restOfData)

def sendToSensor(nodeId, openValve,restOfData):
	toSend = str(VALVE_INSTRUCTION)+" " 				#building the response to send to the border router
	if openValve:
		toSend+="6"						#Macro to open the valve
	else:
		toSend+="7"						#Macro the close the valve
	toSend+=" "+str(nodeId)
	for elem in restOfData:
		toSend+=" " + str(elem)
	toSend+="\n"
	s.sendall(toSend.encode())					#Sending the response
	print("SERVER: "+toSend)

def timeoutCheck():
    for keys in status:
        difference = datetime.datetime.now() - status[keys][1] 		#computing time difference for the timeout
        if  difference.total_seconds() > TIMEOUT:
		del status[keys]					#deleting to node that has a timeout


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
	s.connect((HOST, PORT))						#connection to border router
	line=""	
	while True:
		msg = s.recv(1024)
		if len(msg) <= 0:
			break
		if msg.decode("utf-8") == "<":
				line=""
		line+=msg.decode("utf-8")
		if msg.decode("utf-8") == ">":
			print(line)
			t = Thread(target = assignData, args = (line,)) 	#creating a thread to handle the computation
			t.start()
