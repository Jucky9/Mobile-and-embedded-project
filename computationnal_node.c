#include "contiki.h"
#include "net/rime/rime.h"
#include "random.h"
#include "lib/list.h"
#include "dev/button-sensor.h"
#include "dev/leds.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "macros.h"
#include <string.h>

/* Computationnal node specific MACRO */
#define MAX_ACCEPTED_NODES 5
#define NUMBER_OF_VALUES_FOR_SLOPE 5 //normalement 30
#define THRESHOLD 80

#define NO_MORE_MY_FATHER 3
#define PING_THRESHHOLD 30
/* Initiate variable */

struct node{
	struct node *son;
	linkaddr_t addr;			// Node address
	uint16_t last_rssi;			//The Received Signal Strength Indicator (RSSI)
};

struct processed_node{
	struct processed_node *next;
	linkaddr_t addr;			// Node address
	int timer;					//Represent time since last communication from the node
	int values[NUMBER_OF_VALUES_FOR_SLOPE]; //status of last 30 data received
};

/* The sons_list is a Contiki list that holds the sons seen thus far. */
LIST(sons_list);

/*  All processed nodes list */
LIST(processed_node_list);

/* Broadcast and unicast structure */
static struct broadcast_conn broadcast;
static struct unicast_conn uc;


/* Node father */
static struct node *father = NULL;			//Father node
static bool final_father=false;

/*For the Ping-Pong*/
static int ping_attempts = 0;
static int ping_timer = -1;

/* Connected directly or not to Border Router */
static bool connected_with_border = false;


/*---------------------------------------------------------------------------*/
/* All Process */
PROCESS(broadcast_process, "Broadcast process");
PROCESS(unicast_process, "Unicast process");

AUTOSTART_PROCESSES(&broadcast_process,&unicast_process);

/*---------------------------------------------------------------------------*/
//ALL FUNCTIONS
static void sending_message(int type, linkaddr_t node_address, const linkaddr_t * destination){
	uint8_t msg_to_send[3];
	msg_to_send[0] = type;
	msg_to_send[1] = node_address.u8[0];
	msg_to_send[2] = node_address.u8[1];
	packetbuf_copyfrom(msg_to_send,sizeof(msg_to_send));
	unicast_send(&uc, destination);

}

static void remove_all_children(){
	if(list_length(sons_list)!=0){
		struct node *son = list_head(sons_list);
		while(son!=NULL){
			sending_message(NO_MORE_MY_FATHER,linkaddr_node_addr,&son->addr);
			printf("Removed son %d\n",son->addr.u8[0]);
			son = list_item_next(son);
		}
		list_init(sons_list);
	}
}

static void look_for_new_father(){
	printf("Suppression of father %d\n",father->addr.u8[0]);
	father = NULL;
	final_father = false;
	connected_with_border=false;
	ping_timer = -1;
	ping_attempts = 0;
	remove_all_children();
	uint8_t message_to_send = DISCOVERY_SIGNAL;									//Look again for possible father
	packetbuf_copyfrom(&message_to_send, sizeof(message_to_send));
	broadcast_send(&broadcast);											//Send broadcast to find father
	printf("broadcast message AGAIN sent to JOIN FATHER\n");
}

static bool already_son(uint8_t node_address){
	if(list_length(sons_list)!=0){
		struct node *next_node = list_head(sons_list);
		while(next_node != NULL){
			if(next_node->addr.u8[0]==node_address){
				printf("Node %d in process list!\n",node_address);
				return true;
			}
			next_node = list_item_next(next_node);
		}
	}
	return false;
}

static void reset_ping_info(){
	ping_timer = 0;
	ping_attempts=0;
}

static void remove_son(uint8_t node_address){
	struct node *son = list_head(sons_list);
	while(son->addr.u8[0]!=node_address){
		son = list_item_next(son);
	}
	list_remove(sons_list, son);
	printf("Son %d has been removed successfully!\n",node_address);
}

static void delete_node(uint8_t node_address){
	struct processed_node *proc_node = list_head(processed_node_list);
	bool node_found = false;
	while(!node_found && proc_node != NULL){
		if(proc_node->addr.u8[0]==node_address){
			node_found = true;
			printf("Node %d has been removed successfully\n",proc_node->addr.u8[0]);
			if(already_son(proc_node->addr.u8[0])){
				remove_son(proc_node->addr.u8[0]);
			}
			list_remove(processed_node_list,proc_node);
		}
		proc_node = list_item_next(proc_node);
	}
}

static void resetTimer(uint8_t node_address){
	/* Reset the timer from a processed node */
	if(list_length(processed_node_list)!=0){
		struct processed_node *proc_node = list_head(processed_node_list);
		bool node_found = false;
		while(!node_found && proc_node != NULL){
			if(proc_node->addr.u8[0]==node_address){
				proc_node->timer = 0;
				node_found = true;
			}
			proc_node = list_item_next(proc_node);
		}
	}
}

static bool already_in_process_list(uint8_t node_address){
	if(list_length(processed_node_list)!=0){
		struct processed_node *proc_node = list_head(processed_node_list);
		while(proc_node != NULL){
			if(proc_node->addr.u8[0]==node_address){
				printf("Node %d in process list!\n",node_address);
				return true;
			}
			proc_node = list_item_next(proc_node);
		}
	}
	return false;
}

static void check_timeout(){
	int i=0;
	uint8_t node_to_delete[MAX_ACCEPTED_NODES] = {NULL};
	struct processed_node *proc_node = list_head(processed_node_list);
	while(proc_node!=NULL){
		if(proc_node->timer>18){											//If timer exceed 3 minutes
			node_to_delete[i] = proc_node->addr.u8[0];
			i++;
		}
		proc_node->timer+=1;
		proc_node = list_item_next(proc_node);
	}
	i=0;
	while(node_to_delete[i]!=NULL && i<MAX_ACCEPTED_NODES){
		delete_node(node_to_delete[i]);										//delete the processed node
		i++;
	}
}

static bool computation(uint8_t node_address, int air_data){
	int i;
	if(list_length(processed_node_list)!=0){
		struct processed_node *proc_node = list_head(processed_node_list);
		while(proc_node != NULL){
			if(proc_node->addr.u8[0]==node_address){
				//printf("Found the good node process\n");
				bool openValve=false;
				//int n = sizeof(proc_node->values);
				printf("Air data: %d\n",air_data);
			    for(i=0;i<NUMBER_OF_VALUES_FOR_SLOPE-1;i++){
			        proc_node->values[i]=proc_node->values[i+1];												//shift array to left to append new value
			    }
			    proc_node->values[NUMBER_OF_VALUES_FOR_SLOPE-1]=air_data;
			    bool enough_values = true;
			    for(i=0;i<NUMBER_OF_VALUES_FOR_SLOPE;i++){
			    	if(proc_node->values[i]==-1){
			    		enough_values=false;
			    	}
			    }																								//check if NULL value in array, if there is then there is not enough values
				if(enough_values){
					printf("Enough values\n");
					float x[NUMBER_OF_VALUES_FOR_SLOPE];
					for(i=0;i<NUMBER_OF_VALUES_FOR_SLOPE;i++){x[i]=i;}
					float sumx=0,sumx2=0,sumy=0,sumyx=0,slope=0; 												//needs to be set to zero every time
					for(i=0;i<NUMBER_OF_VALUES_FOR_SLOPE;i++){
					    sumx=sumx+x[i];
					    sumx2=sumx2+x[i]*x[i];
					    sumy=sumy+proc_node->values[i];
					    sumyx=sumyx+proc_node->values[i]*x[i];
				    }
				    slope=(NUMBER_OF_VALUES_FOR_SLOPE*sumyx-sumx*sumy)/(NUMBER_OF_VALUES_FOR_SLOPE*sumx2-sumx*sumx);
				    if(slope<0){
				    	openValve=true;
				    }
				}
				else{
					printf("Not enough values\n");
					if(air_data<THRESHOLD){
						printf("Opening the valve\n");
						openValve=true;
					}
				}
				return openValve;
			}
			proc_node = list_item_next(proc_node);
		}
	}
	return false;																								//should not happen
}

/*---------------------------------------------------------------------------*/
//Receiving structures
static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from){
	uint8_t message_received = *(uint8_t*)packetbuf_dataptr();
	if(connected_with_border && father->addr.u8[0]!=from->u8[0] && message_received==DISCOVERY_SIGNAL){
		sending_message(FATHER_SIGNAL,linkaddr_node_addr,from);										//Propose to be the father
	}
}

static void recv_uc(struct unicast_conn *c, const linkaddr_t *from){
	uint8_t *message_received = packetbuf_dataptr();												//fetch message
	/* If the node is a processed node, reset timer */
	if(message_received[0]!=VALVE_INSTRUCTION){
		if(message_received[0]!=AIR_DATA){
			resetTimer(message_received[1]);
		}
		else{
			resetTimer(message_received[2]);
		}
	}
	//create node structure
	struct node *new_node = malloc(sizeof(struct node));											//Store information about the node communicating
	new_node->last_rssi = abs(packetbuf_attr(PACKETBUF_ATTR_RSSI));									//retrieve signal strength
	linkaddr_copy(&new_node->addr,from);
	//creation complete
	switch (message_received[0]) {
		case FATHER_SIGNAL:																			//Node request to become our father
			if(father==NULL){
				father = new_node;
			}
			else{
				if(!final_father && father->addr.u8[0] != new_node->addr.u8[0] && father->last_rssi > new_node->last_rssi){
					father = new_node;
				}
			}
		break;

		case ACK_FATHER_SIGNAL:
			if(connected_with_border && !already_son(from->u8[0])){
				list_add(sons_list,new_node); 															//Son added to list of son's
			}
			else{
				sending_message(NO_MORE_MY_FATHER,linkaddr_node_addr,&new_node->addr);
			}
		break;

		case NO_MORE_MY_FATHER:
			if(father->addr.u8[0]==from->u8[0]){
				printf("NO_MORE_MY_FATHER signal message received from %d\n",from->u8[0]);
				look_for_new_father();
			} 
		break;

		case PING_TO_FATHER :
			printf("PING_TO_FATHER received by son %d\n",from->u8[0]);
			sending_message(PONG_TO_SON,linkaddr_node_addr,from);
		break;

		case PONG_TO_SON:
			printf("PONG_TO_FATHER received by father %d\n",from->u8[0]);
			reset_ping_info();
		break;

		case AIR_DATA:
			if(list_length(processed_node_list)>=MAX_ACCEPTED_NODES && !already_in_process_list(message_received[2])){
				//Node add itself in the message.
				int msg_to_send_length = packetbuf_datalen();								//get message length
				uint8_t msg_to_send[msg_to_send_length+2];
				int iter;for(iter=0;iter<msg_to_send_length+2;iter++){msg_to_send[iter]=message_received[iter];}
				msg_to_send[msg_to_send_length] = linkaddr_node_addr.u8[0];
				msg_to_send[msg_to_send_length+1] = linkaddr_node_addr.u8[1];
				packetbuf_copyfrom(msg_to_send, sizeof(msg_to_send));										//As the node is not a computational node
				unicast_send(&uc,&father->addr);
			}
			else{
				if(!already_in_process_list(message_received[2])){
					//Add node in the processed list !
					struct processed_node *new_processed_node = malloc(sizeof(struct processed_node));
					new_processed_node->addr.u8[0] = message_received[2];
					new_processed_node->addr.u8[1] = message_received[3];
					new_processed_node->timer = 0;
					int i;for(i=0;i<NUMBER_OF_VALUES_FOR_SLOPE;i++){new_processed_node->values[i]=-1;}
					list_add(processed_node_list,new_processed_node);
				}
				bool openValve = computation(message_received[2],message_received[1]);
				//Copy information
				int msg_to_send_length = packetbuf_datalen();
				uint8_t msg_to_send[msg_to_send_length];
				int iter;for(iter=0;iter<msg_to_send_length;iter++){msg_to_send[iter]=message_received[iter];}
				msg_to_send[0] = VALVE_INSTRUCTION;
				if(openValve){
					msg_to_send[1] = OPEN_VALVE;
				}
				else{
					msg_to_send[1] = CLOSE_VALVE;
				}
				//Copy all packet info
				packetbuf_copyfrom(msg_to_send,sizeof(msg_to_send));
				unicast_send(&uc, from);
			}
		break;

		case VALVE_INSTRUCTION:			//Will never concerned a computational node
			reset_ping_info();
			//Suppress itself from path
			int msg_to_send_length = packetbuf_datalen();		
			uint8_t message_to_send[msg_to_send_length-2];
			int iter;for(iter=0;iter<msg_to_send_length-2;iter++){message_to_send[iter]=message_received[iter];}
			//Send to next concerned son
			linkaddr_t concerned_son;
			concerned_son.u8[0] = message_to_send[msg_to_send_length-2];
			concerned_son.u8[1] = message_to_send[msg_to_send_length-1];
			packetbuf_copyfrom(message_to_send,sizeof(message_to_send));
			unicast_send(&uc, &concerned_son);
		break;
	}

}
/*---------------------------------------------------------------------------*/
//Link structure to calls
static const struct broadcast_callbacks broadcast_call = {broadcast_recv};							//Broadcast structure. All broadcasted message received will be handle by the function broadcast_recv
static const struct unicast_callbacks unicast_callbacks = {recv_uc};								//Unicast structure. All unicast message received will be handle by the function uc_recv

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(broadcast_process, ev, data){
	static struct etimer et;																		//Use to wait a amount of time
	static uint8_t message_to_send; 

	PROCESS_EXITHANDLER(broadcast_close(&broadcast);)

	PROCESS_BEGIN();

	broadcast_open(&broadcast, 129, &broadcast_call);												//Open listener to send the received paquet to the structure broadcast_call

	while(1) {

		etimer_set(&et, CLOCK_SECOND);
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
		if(father==NULL){
			message_to_send = DISCOVERY_SIGNAL;													//Search for a father
			packetbuf_copyfrom(&message_to_send, sizeof(message_to_send));								//Copy into a packet buffer
			broadcast_send(&broadcast);															//Send broadcast
			printf("Looking for a father. Broadcasting request.\n");
		}
	}
	PROCESS_END();
}

PROCESS_THREAD(unicast_process, ev, data){
	static struct etimer et;																	//Use to wait a amount of time. 
	PROCESS_EXITHANDLER(unicast_close(&uc);)
	PROCESS_BEGIN();

	unicast_open(&uc, 146, &unicast_callbacks);													//Open listener to send the received paquet to the structure unicast_call

	while(1){
		etimer_set(&et, CLOCK_SECOND*(10 + (random_rand()%3)));									//Wait around 10-13 seconds seconds (depends on each node)
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
		if(father!=NULL && !final_father){
			sending_message(ACK_FATHER_SIGNAL,linkaddr_node_addr,&father->addr);
			final_father=true;
			connected_with_border=true;
			printf("Father found ! ACK_FATHER_SIGNAL sent to %d\n",father->addr.u8[0]);
			ping_timer=0;																		//Start pinging mechanism
		}

		if(ping_timer!=-1 && final_father && father!=NULL){
			ping_timer+=1;
			if(ping_timer>PING_THRESHHOLD){
				sending_message(PING_TO_FATHER,linkaddr_node_addr,&father->addr);
				ping_attempts+=1;
				if(ping_attempts>3 ){
					look_for_new_father();
				}
			}
		}

		if(list_length(processed_node_list)!=0){
			check_timeout();
		}

	}
	PROCESS_END();
}
